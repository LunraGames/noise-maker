﻿namespace LunraGames.NoiseMaker
{
	[NodeDrawer(typeof(ClampNode), Strings.Modifiers, "Clamp", "Specify an input or ensure boundries are valid.")]
	public class ClampNodeEditor : NodeEditor {}
}