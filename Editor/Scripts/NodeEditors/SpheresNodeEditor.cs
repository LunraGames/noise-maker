﻿namespace LunraGames.NoiseMaker
{
	[NodeDrawer(typeof(SpheresNode), Strings.Generators, "Spheres")]
	public class SpheresNodeEditor : NodeEditor {}
}