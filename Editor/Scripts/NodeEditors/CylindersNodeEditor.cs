﻿using UnityEngine;
using UnityEditor;
using LibNoise;

namespace LunraGames.NoiseMaker
{
	[NodeDrawer(typeof(CylindersNode), Strings.Generators, "Cylinders")]
	public class CylindersNodeEditor : NodeEditor {}
}