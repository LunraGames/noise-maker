﻿namespace LunraGames.NoiseMaker
{
	[NodeDrawer(typeof(DisplaceNode), Strings.Transformers, "Displace", "Specify a source and x, y, and z displacement sources.")]
	public class DisplaceNodeEditor : NodeEditor {}
}