﻿namespace LunraGames.NoiseMaker
{
	[NodeDrawer(typeof(RotatePointNode), Strings.Transformers, "Rotate Point")]
	public class RotatePointNodeEditor : NodeEditor {}
}