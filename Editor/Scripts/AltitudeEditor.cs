﻿namespace LunraGames.NoiseMaker
{
	public abstract class AltitudeEditor
	{
		public abstract Altitude Draw(Altitude altitude, ref bool changed);
	}
}