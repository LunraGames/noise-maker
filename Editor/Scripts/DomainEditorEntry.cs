﻿using System.Collections.Generic;

namespace LunraGames.NoiseMaker
{
	public class DomainEditorEntry
	{
		public DomainDrawer Details;
		public DomainEditor Editor;
	}
}