﻿using UnityEngine;

namespace LunraGames.NoiseMaker
{
	public enum Filtering
	{
		Sphere,
		Plane
	}
}